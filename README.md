# flask-Api

A restful service built in response to [this code challenge](https://gist.github.com/jakedahn/3d90c277576f71d805ed)

*By: Quentin Donnellan*


## Setup
I've tried to make this app reliant on as few external libraries as possible. It does depend on **Flask** and the abstraction layer **Flask-SQLAlchemy** for handling our data. You can optionally install **Nose** for help running tests. All of these are listed in the requirements.txt file and can all be install simply by running:

```
pip install -r requirements.txt
```


## Running the server
After you've completed setup, simply run

```
python runserver.py
```

And the API is live! You can now sends requests to `localhost:5000`

## Running tests
Assuming you've installed Nose (which is included in the requirements file), running tests is as simple as:

```
nosetests
```

I've included unit tests on the model controllers and functional tests for every API method on each endpoint. 

## General Implementation Strategy
A request is mapped to an API endpoint. That API endpoint binds the request to a controller. The controller interacts with the associated database model and based on the controller's response the router returns the appropriate response.

## Some Notes on Design Choices

### HTTP Codes for POST requests to existing data
After some brief research, it turns out there is a bit of discrepancy among community members about the correct code to return for post requests for existing data (the request should obviously be a PUT). Some API's just treat it as a PUT (which I disagree with). The spec's for this problem stated to choose an appropriate code, [I particularly like this SO answer](http://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists), so I've gone with **409: Conflict**

### GET requests to naked `/users` and `/groups`
Although not explicitly stated, I've chosed to return lists of users and groups to the naked urls (i.e. without a specified userid). 

### Posting a user with groups that don't exist
Let's say we post this data to `/users/<userid>`:

```
{
    "first_name": "Taylor"
    "last_name": "Wentz"
    "groups": ["HR", "Planet Labs"]
}
```
If the groups *HR* and *Planet Labs* don't exist, I've chosen to create them. I think this makes more sense than to reject the post request with a "Those groups don't exist" error. 

### Deleting all members from group also deletes group
Since the API response to an empty group and a group that doesn't exist should be the same (404), I've decided to delete the group when a DELETE request is made to `/groups/<groupname>`. I feel like this mates well with my decision to create groups when a user is posted with groupnames that don't yet exist. 

## Closing remarks
I chose not to use Django because I wanted the experience of doing something foreign to me. While Flask is not altogether *foreign*, I've never implemented a REST api on a bare Flask server before (I've done something similar with Flask running on google app engine -but that is with using a ton of other third party libs). I was definitely unfamiliar with the SQLAlchemy abstraction layer and that probably occupied most of my time. In hindsight I don't like my design choice of using a strict schema in the database - what if someone wants to have more than that "first_name" and "last_name" fields in on a User? That would require a healthy rewrite. A schemaless key-value, or even a schema'd key-value store would have been better choices. 