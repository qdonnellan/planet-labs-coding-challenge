from flaskApi.main import app, db
from flaskApi import urls
from flaskApi.models.user import User
from flaskApi.models.group import Group

db.create_all()

app.run(debug=True)