from flaskApi.main import db
from flaskApi.models.group import groups

class User(db.Model):
    userid = db.Column(db.String(80), primary_key=True)
    first_name = db.Column(db.String(80))
    last_name = db.Column(db.String(80))
    groups = db.relationship('Group', secondary=groups,
        backref=db.backref('users', lazy='dynamic'))

    def __init__(self, userid, first_name, last_name, groups=None):
        self.userid = userid
        self.first_name = first_name
        self.last_name = last_name
        if groups is None:
            self.groups = []
        else:
            self.groups = groups