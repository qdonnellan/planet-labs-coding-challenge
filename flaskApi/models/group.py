from flaskApi.main import db

# Need to use this "helper table" to assist SQLAlchemy in establishing
# the many-to-many relationships between groups and users
groups = db.Table('groups',
    db.Column('group_name', db.String(80), db.ForeignKey('group.groupname')),
    db.Column('user_id', db.String(80), db.ForeignKey('user.userid'))
)

class Group(db.Model):
    groupname = db.Column(db.String(80), primary_key=True)