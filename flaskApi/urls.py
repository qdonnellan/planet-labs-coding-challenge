from flaskApi.main import app
from flaskApi.api_endpoints.user_endpoint import UserApi
from flaskApi.api_endpoints.group_endpoint import GroupApi

userView = UserApi.as_view("user_api")
groupView = GroupApi.as_view("group_api")

app.add_url_rule('/users/', view_func = userView)
app.add_url_rule('/users/<userid>', view_func = userView)
app.add_url_rule('/groups/', view_func = groupView)
app.add_url_rule('/groups/<groupname>', view_func = groupView)