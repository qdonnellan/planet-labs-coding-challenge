import os

APP_ROOT = os.path.dirname(os.path.realpath(__file__))
DATABASE_FILENAME = "sampledb"
SQLITE_DATABASE_URI = "sqlite:////%s/%s" % (APP_ROOT, DATABASE_FILENAME)