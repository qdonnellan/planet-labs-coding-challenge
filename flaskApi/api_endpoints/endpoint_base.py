from flask.views import MethodView
from flask import Response
import json

class ApiEndpoint(MethodView):

    def json_response(self, data):
        json_data = json.dumps(data)
        return Response(json_data, mimetype="application/json")