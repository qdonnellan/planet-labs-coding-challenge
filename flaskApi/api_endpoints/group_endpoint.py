from flaskApi.api_endpoints.endpoint_base import ApiEndpoint
from flaskApi.controllers.group_controller import GroupController
from flask import request
import json

class GroupApi(ApiEndpoint):

    def get(self, groupname=None):
        group = GroupController(groupname)
        if not groupname:
            data, status_int = GroupController.get_all_groups(), 200
        elif group.does_groupname_exist() and len(group.get_userids()) > 0:
            data, status_int = group.get_userids(), 200
        else:
            data, status_int = {"error": "group does not exist or has no members"}, 404

        return self.json_response(data), status_int

    def post(self, groupname=None):
        group = GroupController(groupname)
        if not groupname:
            data, status_int = {"error":"groupname required"}, 404
        elif group.does_groupname_exist():
            data, status_int = {"error":"group already exists"}, 409
        elif group.errors:
            data, status_int = group.errors, 400
        else:
            group.create()
            data, status_int = group.get_userids(), 201

        return self.json_response(data), status_int

    def put(self, groupname=None):
        group = GroupController(groupname, request.data)
        if not groupname:
            data, status_int = {"error":"groupname required"}, 400
        elif not group.does_groupname_exist():
            data, status_int = {"error":"group does not exist"}, 404
        elif group.errors:
            data, status_int = group.errors, 400
        else:
            group.update()
            data, status_int = group.get_userids(), 200

        return self.json_response(data), status_int

    def delete(self, groupname=None):
        group = GroupController(groupname)
        if not groupname:
            data, status_int = {"error":"groupname required"}, 400
        elif not group.does_groupname_exist():
            data, status_int = {"error":"group does not exist"}, 404
        else:
            group.delete()
            data, status_int = {"deleted": groupname}, 200

        return self.json_response(data), status_int

