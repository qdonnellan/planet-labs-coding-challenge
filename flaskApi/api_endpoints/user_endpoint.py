from flaskApi.api_endpoints.endpoint_base import ApiEndpoint
from flaskApi.controllers.user_controller import UserController
from flask import request
import json

class UserApi(ApiEndpoint):

    def get(self, userid=None):
        user = UserController(userid)
        if not userid:
            data, status_int = UserController.get_all_users(), 200
        elif user.does_user_exist():
            data, status_int = user.to_dict(), 200
        else:
            data, status_int = {"error": "user does not exist"}, 404

        return self.json_response(data), status_int

    def post(self, userid=None):
        user = UserController(userid, request.data)
        if not userid:
            data, status_int = {"error": "userid required"}, 400
        elif user.does_user_exist():
            data, status_int = {"error" : "userid already exists"}, 409
        elif user.is_request_data_valid():
            user.create()
            data, status_int = user.to_dict(), 201
        else:
            data, status_int = user.errors, 400

        return self.json_response(data), status_int

    def put(self, userid=None):
        user = UserController(userid, request.data)
        if not userid:
            data, status_int = {"error": "userid required"}, 400
        elif not user.does_user_exist():
            data, status_int = {"error": "userid does not exist"}, 404
        elif user.is_request_data_valid():
            user.update()
            data, status_int = user.to_dict(), 200
        else:
            data, status_int = user.errors, 400

        return self.json_response(data), status_int

    def delete(self, userid=None):
        user = UserController(userid)
        if not userid:
            data, status_int = {"error": "userid required"}, 400
        elif not user.does_user_exist():
            data, status_int = {"error": "userid does not exist"}, 404
        else:
            user.delete()
            data, status_int = {"deleted": userid}, 200

        return self.json_response(data), status_int
