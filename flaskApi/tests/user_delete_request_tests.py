from flaskApi.tests.test_base import TestCase
from flaskApi.models.user import User
from flaskApi.models.group import Group
import json

class UserDeleteRequestTests(TestCase):

    sample_data = {"first_name":"Foo","last_name":"Bar","groups":[]}

    def test_delete_request(self):
        response = self.json_post('/users/foouser', self.sample_data)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(User.query.filter_by(userid="foouser").first())

        response = self.app.delete('/users/foouser')
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(User.query.filter_by(userid="foouser").first())

    def test_delete_request_for_bad_userid(self):
        response = self.app.delete('/users/hahahahskjnasjknskjnkjnx')
        self.assertEqual(response.status_code, 404)

    def test_delete_request_for_no_userid(self):
        response = self.app.delete('/users/')
        self.assertEqual(response.status_code, 400)