from flaskApi.models.user import User
from flaskApi.models.group import Group
from flaskApi.main import db
from flaskApi.controllers.group_controller import GroupController
from flaskApi.controllers.user_controller import UserController
from flaskApi.tests.test_base import TestCase

class GroupControllerTest(TestCase):

    def create_two_sample_users(self):
        data = {"first_name":"Foo", "last_name":"Bar", "groups":[]}
        self.user1 = UserController("user1", data)
        self.user2 = UserController("user2", data)
        self.user1.create()
        self.user2.create()

    def test_create_empty_group(self):
        group = GroupController("admins", [])
        group.create()
        saved_group = Group.query.filter_by(groupname="admins").first()
        self.assertIsNotNone(saved_group)

    def test_create_group_of_users(self):
        self.create_two_sample_users()
        group = GroupController("admins", [])
        group.create()

        group = GroupController("admins", ["user1", "user2"])
        group.update()

        admin_group = group.get_group()
        self.assertIn(admin_group, self.user1.get_user().groups)
        self.assertIn(admin_group, self.user2.get_user().groups)

    def test_validate_group_with_users_that_dont_exist(self):
        group = GroupController("admins", ["user99", "userHA"])
        self.assertFalse(group.are_users_valid())
        self.assertIn("user99", group.errors)
        self.assertIn("userHA", group.errors)
        self.assertEqual(group.errors["user99"], "userid does not exist")

    def test_create_group_of_users_then_remove_user(self):
        self.create_two_sample_users()

        group = GroupController("admins", [])
        group.create()

        group = GroupController("admins", ["user1", "user2"])
        group.update()

        group = GroupController("admins", ["user1"])
        group.update()

        admin_group = group.get_group()
        self.assertIn(admin_group, self.user1.get_user().groups)
        self.assertNotIn(admin_group, self.user2.get_user().groups)

    def test_create_group_then_delete_group(self):
        self.create_two_sample_users()

        group = GroupController("admins", [])
        group.create()

        group = GroupController("admins", ["user1", "user2"])
        group.update()

        group.delete()

        admin_group = group.get_group()
        self.assertIsNone(Group.query.filter_by(groupname="admins").first())
        self.assertNotIn(admin_group, self.user1.get_user().groups)
        self.assertNotIn(admin_group, self.user2.get_user().groups)

