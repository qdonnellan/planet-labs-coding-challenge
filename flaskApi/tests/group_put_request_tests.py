from flaskApi.tests.test_base import TestCase
from flaskApi.models.group import Group
from flaskApi.controllers.group_controller import GroupController
from flaskApi.controllers.user_controller import UserController
from flaskApi.main import db

class GroupPutRequestTests(TestCase):

    sample_data = {"first_name":"Foo","last_name":"Bar","groups":[]}

    def test_put_request(self):
        self.json_post('/groups/foogroup', {})
        self.json_post('/users/user1', self.sample_data)
        self.json_post('/users/user2', self.sample_data)

        response = self.json_put('/groups/foogroup', ["user1", "user2"])
        self.assertEqual(response.status_code, 200)

        user1 = UserController('user1')
        self.assertIn("foogroup", user1.to_dict()["groups"])
        user2 = UserController('user1')
        self.assertIn("foogroup", user2.to_dict()["groups"])

    def test_put_request_for_non_existing_group(self):
        response = self.json_put('/groups/HAHAHAHAHHAHAH', {})
        self.assertEqual(response.status_code, 404)
