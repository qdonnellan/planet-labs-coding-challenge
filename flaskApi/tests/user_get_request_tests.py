from flaskApi.tests.test_base import TestCase
from flaskApi.models.user import User
from flaskApi.main import db

class UserGetRequestTests(TestCase):

    def test_request_for_individual_user_that_does_not_exist(self):
        response = self.app.get('/users/foouser')
        self.assertEqual(response.status_code, 404)

    def test_request_for_individual_user_that_does_exist(self):
        new_user = User(userid="foouser", first_name="Foo", last_name="Bar", groups=[])
        db.session.add(new_user)
        db.session.commit()
        response = self.app.get('/users/foouser')
        self.assertEqual(response.status_code, 200)