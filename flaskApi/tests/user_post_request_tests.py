from flaskApi.tests.test_base import TestCase
from flaskApi.models.user import User
import json

class UserPostRequestTests(TestCase):

    sample_data = {"first_name":"Foo","last_name":"Bar","groups":[]}
    
    def test_simple_post_request(self):
        response = self.json_post('/users/foouser', self.sample_data)
        self.assertEqual(response.status_code, 201)

    def test_simple_post_request_on_exsitng_userid(self):
        response = self.json_post('/users/foouser', self.sample_data)
        self.assertEqual(response.status_code, 201)

        response = self.json_post('/users/foouser', self.sample_data)
        self.assertEqual(response.status_code, 409)

    def test_simple_post_request_with_no_userid(self):
        response = self.json_post('/users/', self.sample_data)
        self.assertEqual(response.status_code, 400)