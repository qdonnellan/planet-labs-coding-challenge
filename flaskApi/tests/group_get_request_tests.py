from flaskApi.tests.test_base import TestCase
from flaskApi.models.group import Group
from flaskApi.controllers.group_controller import GroupController
from flaskApi.controllers.user_controller import UserController
from flaskApi.main import db

class GroupGetRequestTests(TestCase):

    

    def test_request_for_individual_group_that_does_not_exist(self):
        response = self.app.get('/groups/foogroup')
        self.assertEqual(response.status_code, 404)

    def test_request_for_individual_group_that_does_exist_and_has_a_member(self):
        new_group = GroupController("newgroup")
        new_group.create()

        sample_data = {"first_name":"Foo","last_name":"Bar","groups":["newgroup"]}
        self.json_post('/users/user1', sample_data)

        response = self.app.get('/groups/newgroup')
        self.assertEqual(response.status_code, 200)

    def test_request_for_group_that_has_no_members(self):
        new_group = GroupController("newgroup")
        new_group.create()

        response = self.app.get('/groups/newgroup')
        self.assertEqual(response.status_code, 404)
