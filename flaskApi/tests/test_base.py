from flaskApi.main import app, db
from flaskApi import urls
import os
import tempfile
import unittest
import json

class TestCase(unittest.TestCase):

    json_header = [('Content-Type', 'application/json')]

    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        app.config['TESTING'] = True
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(app.config['DATABASE'])
        db.session.remove()
        db.drop_all()

    def json_post(self, url, data):
        json_data = json.dumps(data)
        return self.app.post(url, headers=self.json_header, data=json_data)

    def json_put(self, url, data):
        json_data = json.dumps(data)
        return self.app.put(url, headers=self.json_header, data=json_data)