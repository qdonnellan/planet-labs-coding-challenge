from flaskApi.models.user import User
from flaskApi.models.group import Group
from flaskApi.main import db
from flaskApi.controllers.user_controller import UserController
from flaskApi.tests.test_base import TestCase

class UserControllerTest(TestCase):

    def test_is_request_data_valid(self):
        data = {}
        user = UserController('foouser', data)
        self.assertFalse(user.is_request_data_valid())

        data = {"first_name":"Foo"}
        user = UserController('foouser', data)
        self.assertFalse(user.is_request_data_valid())
        self.assertIn("groups", user.errors)
        self.assertIn("last_name", user.errors)

        data = {"first_name":"Foo", "last_name":"Bar", "groups":[]}
        user = UserController('foouser', data)
        self.assertTrue(user.is_request_data_valid())
        

        data = {"first_name":"Foo", "last_name":"Bar", "groups":"not a list"}
        user = UserController('foouser', data)
        self.assertFalse(user.is_request_data_valid())
        self.assertIn("groups", user.errors)

    def test_does_user_exist(self):
        existing_user = User("foouser", "Foo", "Bar", [])
        db.session.add(existing_user)
        db.session.commit()

        user = UserController("foouser")
        self.assertTrue(user.does_user_exist())

        user = UserController("hahahah")
        self.assertFalse(user.does_user_exist())

    def test_get_user(self):
        existing_user = User("foouser", "Foo", "Bar", [])
        db.session.add(existing_user)
        db.session.commit()

        user = UserController("foouser")
        fetched_user = user.get_user()
        self.assertEqual(fetched_user.first_name, "Foo")
        self.assertEqual(fetched_user.last_name, "Bar")

    def test_create(self):
        data = {"first_name":"Foo", "last_name":"Bar", "groups":[]}
        user = UserController("foouser", data)
        user.create()
        self.assertIsNotNone(user.get_user())

    def test_create_user_with_new_groups(self):
        data = {"first_name":"Foo", "last_name":"Bar", "groups":["admins", "proles"]}
        user = UserController("foouser", data)
        user.create()

        group = Group.query.filter_by(groupname = "admins").first()
        self.assertIsNotNone(group)

        group = Group.query.filter_by(groupname = "proles").first()
        self.assertIsNotNone(group)

    def test_update(self):
        data = {"first_name":"Foo", "last_name":"Bar", "groups":[]}
        user = UserController("foouser", data)
        user.create()

        data = {"first_name":"Jimmy", "last_name":"Bar", "groups":[]}
        user = UserController("foouser", data)
        user.update()

        self.assertEqual(user.get_user().first_name, "Jimmy")

    def test_delete(self):
        data = {"first_name":"Foo", "last_name":"Bar", "groups":[]}
        user = UserController("foouser", data)
        user.create()
        self.assertIsNotNone(user.get_user())
        user.delete()
        self.assertIsNone(user.get_user())

    def test_just_for_fun(self):
        self.assertEqual(True, False, 
            """
            Oh hey, thanks for running my tests Planet Labs person!
            I really hope this is the only one that fails...
            """)

