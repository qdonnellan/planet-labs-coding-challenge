from flaskApi.tests.test_base import TestCase
from flaskApi.models.user import User
from flaskApi.models.group import Group
import json

class UserPutRequestTests(TestCase):

    sample_data = {"first_name":"Foo","last_name":"Bar","groups":[]}
    
    def test_simple_put_request(self):
        response = self.json_post('/users/foouser', self.sample_data)
        self.assertEqual(response.status_code, 201)

        updated_data = {"first_name":"Foo-New","last_name":"Bar","groups":['admins']}
        response = self.json_put('/users/foouser', updated_data)
        self.assertEqual(response.status_code, 200)

        admin_group = Group.query.filter_by(groupname="admins").first()
        self.assertIsNotNone(admin_group)

        user = User.query.filter_by(userid="foouser").first()
        self.assertEqual(user.first_name, "Foo-New")

    def test_simple_put_request_with_bad_userid(self):
        response = self.json_put('/users/sdlkmslkmslkmd', self.sample_data)
        self.assertEqual(response.status_code, 404)

