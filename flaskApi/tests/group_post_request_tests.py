from flaskApi.tests.test_base import TestCase
from flaskApi.models.group import Group
from flaskApi.controllers.group_controller import GroupController
from flaskApi.main import db

class GroupPostRequestTests(TestCase):

    def test_post_request(self):
        response = self.json_post('/groups/foogroup', {})
        self.assertEqual(response.status_code, 201)

        group = Group.query.filter_by(groupname="foogroup").first()
        self.assertIsNotNone(group)

    def test_post_request_for_existing_group(self):
        response = self.json_post('/groups/foogroup', {})
        self.assertEqual(response.status_code, 201)

        response = self.json_post('/groups/foogroup', {})
        self.assertEqual(response.status_code, 409)
