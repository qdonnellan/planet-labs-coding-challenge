from flaskApi.tests.test_base import TestCase
from flaskApi.models.group import Group
from flaskApi.controllers.group_controller import GroupController
from flaskApi.controllers.user_controller import UserController
from flaskApi.main import db

class GroupDeleteRequestTests(TestCase):

    def test_group_delete_request(self):
        self.json_post('/groups/foogroup', {})
        group = Group.query.filter_by(groupname="foogroup").first()
        self.assertIsNotNone(group)

        response = self.app.delete("/groups/foogroup")
        self.assertEqual(response.status_code, 200)
        group = Group.query.filter_by(groupname="foogroup").first()
        self.assertIsNone(group)

    def test_delete_unknown_group_requst(self):
        response = self.app.delete("/groups/HAKJNASKJNKJNAKJNS")
        self.assertEqual(response.status_code, 404)

