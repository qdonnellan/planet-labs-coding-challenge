from flaskApi.models.user import User
from flaskApi.models.group import Group
from flaskApi.main import db
import json

class UserController:

    def __init__(self, userid=None, request_data=None):
        self.userid = userid
        if request_data and type(request_data) is not dict:
            self.request_data = json.loads(request_data)
        else:
            self.request_data = request_data
        self.errors = {}

    @staticmethod
    def get_all_users():
        """return a list of all userids"""
        return [user.userid for user in User.query.all()]

    def is_request_data_valid(self):
        self.validate_request_data()
        if self.errors != {}:
            return False
        else:
            return True

    def validate_request_data(self):
        for key in ["first_name", "last_name", "groups"]:
            if key not in self.request_data:
                self.errors[key] = "required field missing"
        
        if "groups" in self.request_data:
            if type(self.request_data["groups"]) is not list:
                self.errors["groups"] = "groups must be a list"

    def get_user(self):
        return User.query.filter_by(userid = self.userid).first()

    def does_user_exist(self):
        if self.get_user() is not None:
            return True
        else:
            return False

    def to_dict(self):
        user = self.get_user()
        data = {
            "userid" : user.userid,
            "first_name" : user.first_name,
            "last_name" : user.last_name,
            "groups" : [x.groupname for x in user.groups]
        }
        return data

    def create(self):
        self.create_groups_if_they_dont_already_exist()
        new_user = User(
            userid = self.userid,
            first_name = self.request_data["first_name"],
            last_name = self.request_data["last_name"],
            groups = self.get_group_list()
        )
        db.session.add(new_user)
        db.session.commit()

    def get_group_list(self):
        groups = []
        for groupname in self.request_data["groups"]:
            groups.append(Group.query.filter_by(groupname=groupname).first())
        return groups

    def create_groups_if_they_dont_already_exist(self):
        for groupname in self.request_data["groups"]:
            if Group.query.filter_by(groupname = groupname).first() is None:
                new_group = Group(groupname=groupname)
                db.session.add(new_group)
                db.session.commit()

    def update(self):
        self.create_groups_if_they_dont_already_exist()
        existing_user = self.get_user()
        existing_user.first_name = self.request_data["first_name"]
        existing_user.last_name = self.request_data["last_name"]
        existing_user.groups = self.get_group_list()
        db.session.commit()

    def delete(self):
        existing_user = self.get_user()
        db.session.delete(existing_user)
        db.session.commit()

