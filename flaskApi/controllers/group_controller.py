from flaskApi.models.user import User
from flaskApi.models.group import Group
from flaskApi.controllers.user_controller import UserController
from flaskApi.main import db
import json

class GroupController:

    def __init__(self, groupname, users=None):
        self.groupname = groupname
        if users and type(users) is not list:
            self.users = json.loads(users)
        else:
            self.users = users
        self.errors = {}

    @staticmethod
    def get_all_groups():
        """return a list of all groupnames"""
        return [group.groupname for group in Group.query.all()]

    def are_users_valid(self):
        self.validate_users()
        if self.errors != {}:
            return False
        else:
            return True

    def validate_users(self):
        if self.users:
            for userid in self.users:
                user = UserController(userid)
                if not user.does_user_exist():
                    self.errors[userid] = "userid does not exist"

    def get_userids(self):
        group = self.get_group()
        return [user.userid for user in group.users]

    def get_group(self):
        return Group.query.filter_by(groupname = self.groupname).first()

    def does_groupname_exist(self):
        if self.get_group() is not None:
            return True
        else:
            return False

    def create(self):
        new_group = Group(groupname = self.groupname)
        db.session.add(new_group)
        db.session.commit()

    def update(self):
        this_group = self.get_group()
        self.remove_instances_of_group_among_existing_users()
        for userid in self.users:
            user = UserController(userid)
            user = user.get_user()
            user.groups.append(this_group)
        db.session.commit()

    def delete(self):
        this_group = self.get_group()
        self.remove_instances_of_group_among_existing_users()
        db.session.delete(this_group)
        db.session.commit()

    def remove_instances_of_group_among_existing_users(self):
        this_group = self.get_group()
        existing_group = self.get_group()
        for user in existing_group.users:
            user.groups.remove(this_group)
        db.session.commit()
