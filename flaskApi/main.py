from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flaskApi import settings

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLITE_DATABASE_URI
db = SQLAlchemy(app)